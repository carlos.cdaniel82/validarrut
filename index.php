<!DOCTYPE html>
<html>
    <head>
        <title>PHP: Valida RUT Chileno - v1.0</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <!-- formulario de contacto -->
        <form action="index.php" method="post" class="form-consulta"> 
            <label>Ingrese RUT: <span>*</span>
                <input type="text" name="rut" placeholder="Ingrese Rut" class="campo-form" required>
            </label>
            <input type="submit" value="Enviar" class="btn-form">
        </form>
        <!-- formulario -->
    </body>
    <?php
    // put your code here
    if ($_POST['rut']) {

        class ValidaRut {

            var $trut;

            function __construct($trut) {
                $this->trut = $trut;
            }

            function validadorRut($trut) {
                $dvt = substr($trut, strlen($trut) - 1, strlen($trut));
                $rutt = substr($trut, 0, strlen($trut) - 1);
                $rut = (($rutt) + 0);
                $pa = $rut;
                $c = 2;
                $sum = 0;

                while ($rut > 0) {
                    $a1 = $rut % 10;
                    $rut = floor($rut / 10);
                    $sum = $sum + ($a1 * $c);
                    $c = $c + 1;
                    if ($c == 8) {
                        $c = 2;
                    }
                }

                $di = $sum % 11;
                $digi = 11 - $di;
                $digi1 = ((string) ($digi));

                if (($digi1 == '10')) {
                    $digi1 = 'K';
                }

                if (($digi1 == '11')) {
                    $digi1 = '0';
                }

                if (($dvt == $digi1)) {
                    ?>
                    <br/>
                    <div class="rut-ok">
                        <?php
                        echo 'El rut es valido: ', $pa, '-', $digi1;
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <br/>
                    <div class="rut-nok">
                        <?php
                        echo 'El rut ingresado ', $pa, '-', $dvt, ' es invalido. Se esperaba: ', $pa,
                        '-', $digi1;
                        ?>
                    </div>
                    <?php
                }
            }

        }

        $miRut = $_POST['rut'];
        $elRut = new ValidaRut($miRut);
        $elRut->validadorRut($miRut);
    }
    ?>
</body>
</html>
